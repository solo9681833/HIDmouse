#include<stm32f30x.h>
#include <hw_config.h>
#include <usb_lib.h>
#include <usb_desc.h>
#include <usb_pwr.h>
#include <stm32f30x_adc.h>
#include <stm32f30x_rcc.h>
#include <stm32f30x_tim.h>
#include <stm32f30x_gpio.h>
#include <stm32f30x_misc.h>
#include<stdbool.h>

__IO uint8_t PrevXferComplete = 1;

RCC_ClocksTypeDef RCC_ClockFreq;

ADC_InitTypeDef hADC;
TIM_TimeBaseInitTypeDef TIM_InitStructure;
GPIO_InitTypeDef GPIO_InitStructure;

volatile bool click = TRUE, pageDown = TRUE, pageUp = TRUE, RMB = TRUE;
volatile int dx = 0,dy = 0;
volatile uint8_t Mouse_Buffer[4] = {0, 0, 0, 0};
volatile int8_t X = 0, Y = 0;

void Gpio() {
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOA, ENABLE);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2 | GPIO_Pin_3| GPIO_Pin_5;  // ����� ������
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_Level_1;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0|GPIO_Pin_4;   // 1�� ����� 1,2 ���
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
}

void InitADC(void)
{
	//������������
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_ADC12,ENABLE);
	RCC_ADCCLKConfig(RCC_ADC12PLLCLK_Div10);

	hADC.ADC_ContinuousConvMode = DISABLE;  //������ ������������ ������
	hADC.ADC_Resolution = ADC_Resolution_12b; //12 ������ ����� ..2^12 ��������
	hADC.ADC_ExternalTrigConvEvent = ADC_ExternalTrigConvEvent_9; //������ �� 9 ������� (������������ 1�� �������)
	hADC.ADC_ExternalTrigEventEdge = ADC_ExternalTrigEventEdge_RisingEdge; //�� ������������� ������
	hADC.ADC_DataAlign = ADC_DataAlign_Right; //������������ �� ������� ����
	hADC.ADC_OverrunMode = DISABLE; //���������� ���������
	hADC.ADC_AutoInjMode = DISABLE; //��������������� ������ ���������
	hADC.ADC_NbrOfRegChannel = 1; //1 ����� ���
	ADC_Init(ADC1,&hADC);
	ADC_Init(ADC2,&hADC);
	ADC_RegularChannelConfig(ADC1,1,1,ADC_SampleTime_7Cycles5); //����� ���, ����� ������, �����, ����� ���������
	ADC_RegularChannelConfig(ADC2,1,1,ADC_SampleTime_7Cycles5); //����� ���, ����� ������, �����, ����� ���������
	ADC_ITConfig(ADC1,ADC_IT_EOC,ENABLE); //���������� ���������� �� �������������� ���
	ADC_ITConfig(ADC2,ADC_IT_EOC,ENABLE);
	ADC_Cmd(ADC1,ENABLE);
	ADC_Cmd(ADC2,ENABLE);
}

void InitTimer(void)
{
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1, ENABLE);
	TIM_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_InitStructure.TIM_Prescaler=36000;
	TIM_InitStructure.TIM_Period=40; //20 ms
	TIM_SelectOutputTrigger(TIM1, TIM_TRGOSource_Update);
	TIM_TimeBaseInit(TIM1, &TIM_InitStructure);
	TIM_Cmd(TIM1,ENABLE);
}

void ADC1_2_IRQHandler()
{
	  dx = (ADC_GetConversionValue(ADC1)>>8);
	  dy = (ADC_GetConversionValue(ADC2)>>8);
	  click = GPIOA->IDR & (1<<1);
	  pageUp = GPIOA->IDR & (1<<2);
	  pageDown = GPIOA->IDR & (1<<3);
	  RMB = GPIOA->IDR & (1<<5);

	  if (!click) Mouse_Buffer[0] = 1;
	  if (!RMB) Mouse_Buffer[0] = -1;
	  if (!pageDown) Mouse_Buffer[3] = -1;
	  if (!pageUp) Mouse_Buffer[3] = 1;

  /* X value calibration */
	  if	(dx < 2) 						X = -18;
	  if	((dx >= 2)	&&	(dx < 4)) 		X = -15;
	  if	((dx >= 4)	&&	(dx < 6)) 		X = -12;
	  if	((dx >= 6)	&&	(dx < 8)) 		X = -9;
	  if	((dx >= 8)	&&	(dx < 10)) 		X = -6;
	  if	(dx == 10)	 					X = -3;
	  //
	  if	(dx==11)						X = 0;
	  //
	  if	(dx == 12) 						X =	4;
	  if	(dx == 13) 						X = 8;
	  if	(dx == 14) 						X = 12;
	  if	(dx == 15)						X = 16;

  /* Y value calibration */
	  if	(dy < 2) 						Y = 18;
	  if	((dy >= 2)	&&	(dy < 4)) 		Y = 15;
	  if	((dy >= 4)	&&	(dy < 6)) 		Y = 12;
	  if	((dy >= 6)	&&	(dy < 8)) 		Y = 9;
	  if	((dy >= 8)	&&	(dy < 10)) 		Y = 6;
	  if	(dy == 10)	 					Y = 3;
	  //
	  if	(dy==11)						Y = 0;
	  //
	  if	(dy == 12) 						Y = -4;
	  if	(dy == 13) 						Y = -8;
	  if	(dy == 14) 						Y = -12;
	  if	(dy == 15)						Y = -16;

	 /* prepare buffer to send */
	    Mouse_Buffer[1] = X;
	    Mouse_Buffer[2] = Y;

	  /* Reset the control token to inform upper layer that a transfer is ongoing */
	  PrevXferComplete = 0;

	  /* Copy mouse position info in ENDP1 Tx Packet Memory Area*/
	  USB_SIL_Write(EP1_IN, Mouse_Buffer, 4);

	  /* Enable endpoint for transmission */
	  SetEPTxValid(ENDP1);

	  ADC_ClearITPendingBit(ADC1,ADC_IT_EOC);
	  Mouse_Buffer[0] = 0;
	  Mouse_Buffer[3] = 0;

}

int main(void)
{
	Set_System();
	Set_USBClock();
	USB_Interrupts_Config();
	USB_Init();
	Gpio();
	InitADC();
	InitTimer();
	NVIC_EnableIRQ(ADC1_2_IRQn);
	ADC_StartConversion(ADC1);
	ADC_StartConversion(ADC2);
    while(1)
    {

    }
}
